package main

import (
	"encoding/json"
	"log"
	"net/http"
	"os"
	"strconv"
	"strings"
	"time"
)

type response struct {
	Code      int    `json:"code"`
	UserID    string `json:"user_id"`
	ExpiresAt int64  `json:"expires_at"`
}

type request struct {
	Token string `json:"token"`
}

func handle(w http.ResponseWriter, req *http.Request) {
	const tag = "[handle]"
	reqBody := request{}
	if err := json.NewDecoder(req.Body).Decode(&reqBody); err != nil {
		log.Printf(tag+" error while decoding request: %v", err)
		w.WriteHeader(http.StatusForbidden)
		return
	}
	_ = req.Body.Close()

	log.Printf(tag + " read body completed, processing...\n")

	parts := strings.SplitN(reqBody.Token, ":", 1)
	code := 200
	if _code, err := strconv.ParseInt(parts[0], 10, 64); err == nil && _code >= 200 && _code < 600 {
		code = int(_code)
	}
	userID := parts[0]
	if len(parts) > 1 {
		userID = parts[1]
	}

	resp := &response{
		Code:      code,
		UserID:    userID,
		ExpiresAt: time.Now().AddDate(0, 0, 1).Unix(),
	}

	log.Printf(tag+" write header code: %d\n", code)
	w.WriteHeader(code)
	if err := json.NewEncoder(w).Encode(resp); err != nil {
		log.Printf("failed to encode response: %v", err)
	}

	return
}

func main() {
	port := "4041"
	if _port := os.Getenv("PORT"); _port != "" {
		port = _port
	}
	http.HandleFunc("/", handle)

	log.Printf("start listening on :%s\n", port)
	if err := http.ListenAndServe("0.0.0.0:"+port, nil); err != nil {
		log.Fatalf("failed to listen: %v", err)
	}
}
